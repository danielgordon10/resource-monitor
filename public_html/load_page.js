$(document).ready(function() { 
    function refresh() {
        $.ajax({
            url : "usage_current.html",
            success : function(result){
                if (result.length > 0) {
                    $(document.body).html(result);
                }
            }
        });
    } 
    refresh();
    window.setInterval(refresh, 2000);
});
