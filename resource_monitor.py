#!/usr/bin/env python

import hashlib
import multiprocessing
import os
import subprocess
import time
import traceback

try:
    import cPickle as pickle
except:
    import pickle

URL = 'YOUR.FULL.URL.com'
ACCOUNT_NAME = 'USER_NAME'

PAGE_WIDTH = 700
BAR_HEIGHT = 20
FONT_SIZE = 5


def index(ll, f, i=0):
    j = i
    while j < len(ll) and not f(ll[j]):
        j += 1
    if j >= len(ll):
        return -1
    return j


def to_bytes(bb, ll):
    ll = ll.lower()
    if ll.startswith('k'):
        return bb << 10
    elif ll.startswith('m'):
        return bb << 20
    elif ll.startswith('g'):
        return bb << 30
    else:
        return bb


def box_text(width, height, color=None, name=None):
    if name is not None:
        hex_digest = int(hashlib.sha1(name).hexdigest(), 16)
        r = int(hex_digest % 255)
        g = int((hex_digest / 255) % 255)
        b = int((hex_digest / 255 ** 2) % 255)
        return (
                '<div data-tooltip="%s" style="display: inline-block; width: %dpx; height: %dpx; background-color: rgb(%d, %d, %d);"></div>'
                % (name, int(round(width)), int(round(height)), r, g, b))
    else:
        return ('<div style="display: inline-block; width: %dpx; height: %dpx; background-color: %s;"></div>'
                % (int(round(width)), int(round(height)), color))


def main():
    # Get GPU info.
    n_gpus = len([x for x in subprocess.check_output(['nvidia-smi', '-L']).split('\n') if len(x.strip()) > 0])
    gpu_max_mem = [0] * n_gpus
    gpu_pids = [None] * n_gpus
    for i in range(n_gpus):
        lines = [x.strip() for x in
                 subprocess.check_output(['nvidia-smi', '-i', str(i), '-q', '-d', 'MEMORY']).split('\n') if
                 len(x.strip()) > 0]
        j = index(lines, lambda x: x.startswith('FB Memory Usage'))
        j = index(lines, lambda x: x.startswith('Total'), i=j + 1)
        bb, ll = lines[j].split()[-2:]
        bb = int(bb)
        gpu_max_mem[i] = to_bytes(bb, ll)

        lines = [x.strip() for x in
                 subprocess.check_output(['nvidia-smi', '-i', str(i), '-q', '-d', 'PIDS']).split('\n') if
                 len(x.strip()) > 0]
        j = 0
        pidxs = []
        while j >= 0:
            j = index(lines, lambda x: x.startswith('Process ID'), i=j)
            if j > 0:
                pidxs.append(j)
                j += 1
        gpu_pids[i] = []

        for k in range(len(pidxs)):
            j = pidxs[k]
            nj = pidxs[k + 1] if k < len(pidxs) - 1 else len(lines)
            pid = int(lines[j].split()[-1])
            mem_idx = index(lines, lambda x: x.startswith('Used GPU Memory'), i=j + 1)
            if mem_idx < 0 or mem_idx >= nj:
                continue
            bb, ll = lines[mem_idx].split()[-2:]
            bb = int(bb)
            mem_usage = to_bytes(bb, ll)
            gpu_pids[i].append((pid, mem_usage))

    # Now get the set of all users.
    all_users = sorted(os.listdir('/home'))

    # Compile usage information.
    usage = {}
    for user in all_users + ['other']:
        usage[user] = {}
        usage[user]['mem'] = int(0)
        usage[user]['cpu'] = 0.0
        for i in range(n_gpus):
            usage[user]['gpu_%d' % i] = int(0)

    procs = [x.strip().split() for x in
             subprocess.check_output(['ps', '-eo', 'pid,user,rss,%cpu', '--no-headers']).split('\n') if
             len(x.strip()) > 0]
    active_users = set()
    procs_by_pid = {}
    for proc in procs:
        if proc[1] not in all_users:
            proc[1] = 'other'
        usage[proc[1]]['mem'] += to_bytes(int(proc[2]), 'k')
        usage[proc[1]]['cpu'] += float(proc[3])
        procs_by_pid[int(proc[0])] = proc
        if usage[proc[1]]['mem'] > 0 or usage[proc[1]]['cpu'] > 0.0:
            active_users.add(proc[1])

    # Disk partition stuff
    try:
        disk = subprocess.check_output('sudo repquota -a', shell=True).split('\n')
        idx = [i for i, x in enumerate(disk) if x.startswith('-------')][0]
        usage['other']['disk'] = 0
        for line in [x.strip() for x in disk[(idx + 1):] if len(x.strip()) > 0]:
            ll = line.split()
            user, du = ll[0], to_bytes(int(ll[2]), 'k')
            if user in all_users:
                usage[user]['disk'] = du
                # Only add to active users for disk usage if the user is using at least 1GB.
                if du > 10 ** 9:
                    active_users.add(user)
            else:
                usage['other']['disk'] = usage['other']['disk'] + du
    except:
        pass

    for i in range(n_gpus):
        for pid, gpu_mem in gpu_pids[i]:
            if pid not in procs_by_pid:
                raise Exception('Unable to find process with pid %d in list of processes returned by ps.' % pid)
            user = procs_by_pid[pid][1]
            usage[user]['gpu_%d' % i] += gpu_mem
            if usage[user]['gpu_%d' % i] > 0:
                active_users.add(user)

    page_html = []
    recently_used_path = '/home/' + ACCOUNT_NAME + '/public_html/recent_usage.pkl'
    recently_used_data = [{} for _ in range(n_gpus)]
    if os.path.exists(recently_used_path):
        try:
            recently_used_data = pickle.load(open(recently_used_path, 'rb'))
            os.remove(recently_used_path)
        except:
            pass

    page_html.append('<div style="font-size: 50px; padding-bottom: 20px"><a target="_parent" href="http://' +
                     URL + '/~' + ACCOUNT_NAME + '/usage.html">' + URL + '</a></div>')
    page_html.append('<div style="padding-right: 50px;">')
    # List all active users.
    for user in all_users:
        if user in active_users:
            page_html.append('%s <font size="%d"><b>%s</b></font><br>\n' % (
                box_text(0.05 * PAGE_WIDTH, 0.8 * BAR_HEIGHT, name=user), FONT_SIZE, user))
    page_html.append('%s <font size="%d"><b>%s</b></font><br>\n' % (
        box_text(0.05 * PAGE_WIDTH, 0.8 * BAR_HEIGHT, 'Black'), FONT_SIZE, 'Other'))

    page_html.append('%s<br>\n' % box_text(100, 0.5 * BAR_HEIGHT, 'White'))

    # GPU usage
    for i in range(n_gpus):
        total = gpu_max_mem[i] / (2.0 ** 30)
        used = sum([user['gpu_%d' % i] for user in usage.values()]) / (2.0 ** 30)
        page_html.append('<font size="%d"><b>GPU %d:</b> %.2f / %.2f GB </font><br>\n' % (FONT_SIZE, i, used, total))
        page_html.append('<div style="display: block; width: %dpx; height: %dpx; border: 2px solid black;">' % (
            PAGE_WIDTH, BAR_HEIGHT))
        for user in all_users + ['other']:
            gpu_mem = usage[user]['gpu_%d' % i]
            if gpu_mem > 0:
                if user in all_users:
                    page_html.append(box_text(1.0 * gpu_mem / gpu_max_mem[i] * PAGE_WIDTH, BAR_HEIGHT, name=user))
                else:
                    page_html.append(box_text(1.0 * gpu_mem / gpu_max_mem[i] * PAGE_WIDTH, BAR_HEIGHT, 'Black'))

                if user in all_users:
                    if user not in recently_used_data[i]:
                        recently_used_data[i][user] = 0
                    recently_used_data[i][user] = (time.time(), '0 minutes ago')
            else:
                if user in all_users:
                    if user in recently_used_data[i]:
                        prev_time, _ = recently_used_data[i][user]
                        diff = time.time() - prev_time
                        if diff > 60 * 60:
                            del recently_used_data[i][user]
                        else:
                            minutes_ago = int(diff / 60)
                            if minutes_ago == 1:
                                m_str = '1 minute ago'
                            else:
                                m_str = '%d minutes ago' % minutes_ago
                            recently_used_data[i][user] = (prev_time, m_str)

        page_html.append('</div>\n')
        if len(recently_used_data[i]) > 0:
            page_html.append('<font size="%d">\nRecently Used By: %s\n</font>\n<br>\n' % (FONT_SIZE, ', '.join(
                ['%s: %s' % (key, recently_used_data[i][key][1])
                 for key in sorted(recently_used_data[i].keys(), key=lambda kk: recently_used_data[i][kk])])))
        page_html.append('<br>\n')

    pickle.dump(recently_used_data, open(recently_used_path, 'wb'))

    page_html.append('%s<br>\n' % box_text(100, 0.5 * BAR_HEIGHT, 'White'))

    # CPU usage
    total = multiprocessing.cpu_count() * 100.0
    used = sum([user['cpu'] for user in usage.values()])
    page_html.append('<font size="%d"><b>CPU:</b> %.2f / %.2f %% </font><br>\n' % (FONT_SIZE, used, total))
    page_html.append(
        '<div style="display: block; width: %dpx; height: %dpx; border: 2px solid black;">' % (PAGE_WIDTH, BAR_HEIGHT))
    for user in all_users + ['other']:
        cpu = usage[user]['cpu']
        if cpu > 0:
            if user in all_users:
                page_html.append(box_text(1.0 * cpu / total * PAGE_WIDTH, BAR_HEIGHT, name=user))
            else:
                page_html.append(box_text(1.0 * cpu / total * PAGE_WIDTH, BAR_HEIGHT, 'Black'))
    page_html.append('</div><br>\n')

    # Memory usage
    total = int(os.sysconf('SC_PAGE_SIZE')) * int(os.sysconf('SC_PHYS_PAGES'))
    used = sum([user['mem'] for user in usage.values()]) / (2.0 ** 30)
    page_html.append(
        '<font size="%d"><b>System Memory:</b> %.2f / %.2f GB </font><br>\n' % (FONT_SIZE, used, total / (2.0 ** 30)))
    page_html.append(
        '<div style="display: block; width: %dpx; height: %dpx; border: 2px solid black;">' % (PAGE_WIDTH, BAR_HEIGHT))
    for user in all_users + ['other']:
        mem = usage[user]['mem']
        if mem > 0:
            if user in all_users:
                page_html.append(box_text(1.0 * mem / total * PAGE_WIDTH, BAR_HEIGHT, name=user))
            else:
                page_html.append(box_text(1.0 * mem / total * PAGE_WIDTH, BAR_HEIGHT, 'Black'))
    page_html.append('</div><br>\n')

    # Disk usage
    # import IPython
    # IPython.embed()
    # if 'disk' in user:
    total_space = subprocess.check_output('df -l', shell=True).split('\n')
    for line in [x.strip() for x in total_space if len(x.strip()) > 0]:
        ll = [x.strip() for x in line.split()]
        if ll[-1] == '/':
            total_disk_space = to_bytes(int(ll[1]), 'k')
            break
    used = sum([user['disk'] for user in usage.values() if 'disk' in user]) / (2.0 ** 30)
    page_html.append('<font size="%d"><b>Disk Usage:</b> %.2f / %.2f GB </font><br>\n' % (
        FONT_SIZE, used, total_disk_space / (2.0 ** 30)))
    page_html.append(
        '<div style="display: block; width: %dpx; height: %dpx; border: 2px solid black;">' % (PAGE_WIDTH, BAR_HEIGHT))
    for user in all_users + ['other']:
        if 'disk' not in usage[user]:
            continue
        du = usage[user]['disk']
        if du > 0:
            if user in all_users:
                page_html.append(box_text(1.0 * du / total_disk_space * PAGE_WIDTH, BAR_HEIGHT, name=user))
            else:
                page_html.append(box_text(1.0 * du / total_disk_space * PAGE_WIDTH, BAR_HEIGHT, 'Black'))
    page_html.append('</div><br>\n')

    page_html.append('</div>')

    # Write raw nvidia-smi output to file
    smi_output = subprocess.check_output(['nvidia-smi'])
    page_html.append('<div style="padding-top: 10px;">')
    page_html.append('<pre>\n')
    page_html.append(smi_output + '\n')
    page_html.append('</pre>')
    page_html.append('</div>')

    fp = open('/home/' + ACCOUNT_NAME + '/public_html/usage_current.html', 'w')
    fp.write(''.join(page_html))
    fp.close()


if __name__ == '__main__':
    try:
        main()
    except:
        traceback.print_exc()
