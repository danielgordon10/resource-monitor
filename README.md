# Server Status Page
![](images/screenshot.png)

## Setup
1. Clone this repository.
1. Run these commands
    ```bash
    sudo chmod -R 777 public_html
    sudo chmod 755 resource_monitor*
    sudo apt-get install apache2
    sudo a2enmod userdir
    sudo service apache2 restart
    ```
1. Edit [resource_monitor.sh](resource_monitor.sh) line `python /home/USER_NAME/resource_monitor.py` to point to the actual file path.
1. Edit [resource_monitor.py](resource_monitor.py) lines `URL = 'YOUR.FULL.URL.com` and `'ACCOUNT_NAME = 'USER_NAME'` to be the actual values.
1. Run
    ```bash
   cp -r public_html ~ 
   ```
1. Try `python resource_monitor.py` and `sh resource_monitor.sh`. `resource_monitor.py` will run once and will show any errors in the python script. `resource_monitor.sh` will run forever, but you should see updated versions on the usage page.

### Ubuntu 16.04 (Probably works with newer Ubuntu as well):
1. In [resource_monitor.service](resource_monitor.service) edit the line that says `ExecStart=/home/USER_NAME/resource_monitor.sh` to point to the location of [resource_monitor.sh](resource_monitor.sh).
1. Run 
    ```bash
    sudo cp resource_monitor.service /etc/systemd/system/
    sudo systemctl enable resource_monitor 
    sudo systemctl restart resource_monitor
    sudo systemctl status resource_monitor
    ```
   
### Ubuntu 14.04 or older:
1. In [resource_monitor.conf](resource_monitor.conf) edit the line that says `python /home/USER_NAME/resource_monitor.py` to point to the location of [resource_monitor.py](resource_monitor.py).
1. Run
    ```bash
    sudo cp resource_monitor.conf /etc/init/
    sudo service resource_monitor restart
    ```

### (Optional) Setup disk usage monitoring
1. If you didn't already, run
   ```bash
    sudo apt-get install quota
   ``` 
1. Open /etc/fstab with root permissions (e.g., run sudo gedit /etc/fstab). Add "grpquota" and "usrquota" to the list of parameters in the 4th column for any drive you want to monitor usage. Here's an example:
![](images/usrquota.png)
1. You will need to restart the computer for this to take effect.
